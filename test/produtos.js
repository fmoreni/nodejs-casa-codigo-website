var express = require('../config/express')();
var request = require('supertest')(express);

describe('#ProdutosController', function() {
	
	beforeEach(function(done){
		// pesquisar node-database-clean
		var conn = express.infra.connectionFactory();
		conn.query("delete from livros", function(ex, result){
			if(!ex){
				done();
			} else {
				console.log(ex);
			}
		});
	});
	
	
	it('#listagem json',function(done){
		request.get('/produtos/')
			.set('Accept','application/json')
			.expect('Content-Type',/json/)
			.expect(200, done);
		
	});
	
	
	it("#cadastro produto com dados inválidos", function (done) {
		request.post('/produtos')
			.send({titulo:'', descricao:"Novo"})
			.expect(400,done);
		
	});
	
	it("#cadastro produto com dados validos", function (done) {
		request.post('/produtos')
			.send( { titulo:'Titulo Teste', descricao: 'Novo 1' , preco: 20.12 } )
			.expect(302,done);
		
	});
	
	
	
	
});