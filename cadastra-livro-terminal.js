var http = require('http');

var configs = {
	hostname: 'localhost',
	port: 3000,
	path: '/produtos',
	method: 'post',
	headers: {
		'Accept': 'application/json',
		'Content-type': 'application/json'
	}
};

var client = http.request(configs, function(response){
	
	response.on('data',function(body){
		console.log('Corpo:' + body);
	});
	
});

var produto = {
	titulo: '',
	descricao: 'Node, JavaScript',
	preco: 100
}

client.end(JSON.stringify(produto));