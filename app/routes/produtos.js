module.exports = function(app){
	app.get('/produtos',function(request,response){
		
		var connection = app.infra.connectionFactory();
		var produtosDAO = new app.infra.ProdutosDAO(connection);
		
		produtosDAO.list(function(err, results, next){
			
			//if(err){
			//	return next(err);
			//}
			
			response.format({
				html: function(){
					response.render("produtos/list", {list: results});	
				},
				json: function(){
					response.json(results);
				}
			});
			
		});
		
		connection.end();
		
	});
	
	
	app.get('/produtos/new',function(request,response) {
		
		response.render("produtos/new", {erros: {}, produto: {} } );
		
	});
	

	app.post('/produtos', function(request,response){
		
		var produto = request.body;
		
		
		// Data Validator
		request.assert('titulo','Título é Obrigatório').notEmpty();
		request.assert('preco','Formato Inválido').isFloat();
		
		var erros = request.validationErrors();
		if (erros){
			
			response.format({
				html: function(){
					response.status(400).render("produtos/new", {erros: erros, produto: produto});	
				},
				json: function(){
					response.status(400).json(erros);
				}
				
			});
			
			return;
		}

		var connection = app.infra.connectionFactory();
		var produtosDAO = new app.infra.ProdutosDAO(connection);

		produtosDAO.add(produto, function(err, results){
			response.redirect('/produtos');
		});	
		
	});
	
	
	

	
	
}